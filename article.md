---
title: "23 let českého hospodaření: přebytkový byl Klaus. Zeman-premiér byl v mínusu"
perex: "Návrh rozpočtu na příští rok, který ve středu večer schválila sněmovna, počítá se schodkem 60 miliard korun. Ještě před hlasováním komentoval prezident Miloš Zeman finanční plány Sobotkova kabinetu slovy: „...v době prosperity má být rozpočet přebytkový.“ Jak vypadala rozpočtová bilance za jeho vlády? A jak vysoké schodky schválili ostatní čeští premiéři?"
description: "Návrh rozpočtu na příští rok, který ve středu večer schválila sněmovna, počítá se schodkem 60 miliard korun. Jak vysoké deficity schválili čeští premiéři v minulých letech?"
authors: ["Michal Zlatkovský", "Petr Kočí"]
published: "8. listopadu 2016"
url: "rozpocet"
libraries: [jquery, "https://code.highcharts.com/highcharts.js"]
socialimg: https://interaktivni.rozhlas.cz/rozpocet/media/socimg.png
recommended:
  - link: https://interaktivni.rozhlas.cz/duvera-prezidentum/
    title: Zeman je prezidentem voličů ČSSD, KSČM a ANO. Vadí TOP 09 a Pirátům
    perex: Detailní analýza průzkumů veřejného mínění ukazuje, kdo jsou prezidentovi věrní. U Zemana jsou to voliči levice, Klausovi věřili vedle ODS také zelení.
    image: https://interaktivni.rozhlas.cz/duvera-prezidentum/media/socialimg.png
  - link: https://interaktivni.rozhlas.cz/krajske-volby-v-okrscich/
    title: Jak volili vaši sousedi?
    perex: Prohlédněte si nejpodrobnější mapu volebních výsledků
    image: https://interaktivni.rozhlas.cz/media/51445ce4a3c687cbb2d35b797e18e359/600x_.jpg
  - link: https://interaktivni.rozhlas.cz/hazard/
    title: Hazardní byznys roste. Automaty mizí, nahrazují je nebezpečné online sázky
    perex: Čeští hazardní hráči loni vsadili rekordních 152 miliard korun. Trh s rizikem roste navzdory úspěšné regulaci hracích automatů. Mladší generace hráčů teď má problém, který neumí nikdo řešit.
    image: https://interaktivni.rozhlas.cz/hazard/media/2521b6ae119fecada5488c82f544af65/1600x_.jpeg
---

Od vzniku samostatné České republiky sestavila vláda přebytkový rozpočet jen v prvních třech letech za premiéra Václava Klause. Od té doby končí Česko každý rok s větším či menším deficitem. Ten vůbec největší, 192,4 miliardy, zatížil stát uprostřed ekonomické krize za vlády Mirka Topolánka.

<div data-bso=1></div>

<aside class="big">
  <div id="graf1"></div>
</aside>

<aside class="big">
  <div id="graf4"></div>
</aside> 

Vzhledem k tomu, že hodnota koruny se během let například vlivem inflace nebo rostoucího HDP mění, nelze přesně poměřovat schodky v jednotlivých letech absolutními čísly. Při pohledu na schodky rozpočtů vyjádřených v procentech rozpočtových příjmů je srovnání snadnější. Zatímco jsou schodky například v letech 2001 a 2015 srovnatelné (68 a 63 miliard), v prvním případě tvořil deficit necelou desetinu rozpočtových příjmů, v druhém však méně než pět procent.

<aside class="big">
  <div id="graf2"></div>
</aside>

Součástí státního rozpočtu přitom nejsou zisky ze státem spoluvlastněných podniků a příjmy z privatizace. To ovlivňuje skutečnou bilanci zejména vlády Miloše Zemana. Přesná čísla nejsou známá, ale sám Miloš Zeman v roce 2000 odhadl výnos z privatizace až na [500 miliard](http://ekonomika.idnes.cz/milos-zeman-privatizace-nevynese-200-ale-500-miliard-korun-pio-/ekonomika.aspx?c=A000320175357ekonomika_jjx).  

Do roku 1998 vlády počítaly s vyrovnaným rozpočtem, se zvětšujícím se schodkem se ale situace změnila. Plánovaná bilance rozpočtu se často se skutečností rozchází - nejvíce v už zmiňovaném krizovém roce 2009.

<aside class="big">
  <div id="graf3"></div>
</aside>
var data = {
  "rok":[1993,1994,1995,1996,1997,1998,1999,2000,2001,2002,2003,2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017],
  "plan_prijmy":[342.2,381.8,411.746,497.641,549.1,536.635,574.112,592.156,636.1974,693.399679,686.062391,754.080652,824.830643,889.391799,949.47742,1036.51054,1114.001697,1022.21935,1055.700908,1084.700778,1076.367603,1098.237009,1118.455071,1180.856787,1249.3],
  "plan_vydaje":[342.2,381.8,411.746,497.641,549.1,536.635,605.127,627.336,685.1773,739.622657,807.962391,869.050652,908.415643,973.101765,1040.77742,1107.31054,1166.181282,1184.91935,1190.700908,1189.700778,1176.367603,1210.237009,1218.455071,1250.856787,1309.3],
  "plan_bilance":[0,0,0,0,0,0,-31.015,-35.18,-48.9799,-46.222978,-121.9,-114.97,-83.585,-83.709966,-91.3,-70.8,-52.179585,-162.7,-135,-105,-100,-112,-100,-70,-60],
  "real_prijmy":[358,390.507786,439.968,482.816965,508.95,537.411,567.275,586.208,626.223,705.043,699.665037,769.207443,866.460149,923.059944,1025.882854,1063.941031,974.614683,1000.377095,1012.755373,1051.386869,1091.863396,1133.825908,1234.52,{y:1180.856787, color:'#87B2D4'},{y:1249.3, color:'#87B2D4'}],
  "real_vydaje":[356.919,380.058956,432.738,484.379239,524.668,566.741,596.909,632.268,693.921,750.758,808.718373,862.891674,922.798001,1020.640217,1092.274513,1083.943643,1167.00905,1156.793483,1155.526204,1152.386677,1173.127823,1211.608153,1297.32,{y:1250.856787, color:'#F39899'},{y:1309.3, color:'#F39899'}],
  "real_bilance":[1.081,10.44883,7.23,-1.562274,-15.718,-29.33,-29.634,-46.06,-67.698,-45.715,-109.053336,-93.684231,-56.337852,-97.580273,-66.391659,-20.002612,-192.394368,-156.416388,-142.770831,-100.999808,-81.264427,-77.782245,-62.8,61.77,-60],
  "bilance_pct":[0.3028698388,2.749265564,1.670756901,-0.3225311645,-2.995799248,-5.175203488,-4.96457584,-7.284885523,-9.755865581,-6.089179203,-13.48471107,-10.8570095,-6.105112055,-9.560692531,-6.078294258,-1.845355349,-16.48610763,-13.52154817,-12.3554819,-8.76440261,-6.927158781,-6.419752525,-4.84074862,-4.796712191,-4.80268951],
  "premier":["klaus","klaus","klaus","klaus","klaus","tosovsky","zeman","zeman","zeman","zeman","spidla","spidla","gross","paroubek","topolanek","topolanek","topolanek","fischer","necas","necas","necas","rusnok","sobotka","sobotka"],
  "minfin":["kocarnik","kocarnik","kocarnik","kocarnik","kocarnik","pilip","svoboda","mertlik","rusnok","rusnok","sobotka","sobotka","sobotka","sobotka","tlusty","kalousek","kalousek","janota","kalousek","kalousek","kalousek","fischer","babis","babis"]}

  $(function () {
    Highcharts.setOptions({
       lang: {
         decimalPoint: ","
        }
    })
    Highcharts.chart('graf1', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Dvaadvacet schodků a tři přebytky: Vývoj skutečné bilance českého státního rozpočtu'
      },
      xAxis: {
        categories: data.rok,
        crosshair: true,
        plotBands: [
        {
          from: -0.5,
          to: 4.5,
          color: '#f0f0f0',
          label: {
            text: "Klaus"
          }
        },
        {
          from: 4.5,
          to: 5.5,
          color: '#ffffff',
          label: {
            text: "Tošovský",
            y: -5
          }
        },
        {
          from: 5.5,
          to: 9.5,
          color: '#f0f0f0',
          label: {
            text: "Zeman"
          }
        },
        {
          from: 9.5,
          to: 11.5,
          color: '#ffffff',
          label: {
            text: "Špidla"
          }
        },
        {
          from: 11.5,
          to: 12.5,
          color: '#f0f0f0',
          label: {
            text: "Gross"
          }
        },
        {
          from: 12.5,
          to: 13.5,
          color: '#ffffff',
          label: {
            text: "Paroubek",
            y:-5
          }
        },
        {
          from: 13.5,
          to: 16.5,
          color: '#f0f0f0',
          label: {
            text: "Topolánek"
          }
        },
        ,
        {
          from: 16.5,
          to: 17.5,
          color: '#ffffff',
          label: {
            text: "Fischer",
            y: -5
          }
        },
        {
          from: 17.5,
          to: 20.5,
          color: '#f0f0f0',
          label: {
            text: "Nečas"
          }
        },
        {
          from: 20.5,
          to: 21.5,
          color: '#ffffff',
          label: {
            text: "Rusnok",
            y: -5
          }
        },
        {
          from: 21.5,
          to: 24.5,
          color: '#f0f0f0',
          label: {
            text: "Sobotka"
          }
        }
        ],
      },
      yAxis: {
        title: {
          text: "miliardy korun"
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      tooltip: {
        shared: true,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> skutečná bilance rozpočtu: <b>{point.y:,.2f}</b> mld.<br/>'
      },      
      plotOptions: {
        column: {
          borderWidth: 0,
          dataLabels: {
            enabled: true,
            formatter: function () {
              if (this.y==-60) return "plán";
              if (this.y==-70) return "plán";
              if (this.y<-190) return "schodek 192,4 mld.";
              if (this.y>10) return "přebytek 10,4 mld.";
            }
          }
        }
      },
      series: [{
        name: 'skutečná bilance státního rozpočtu',
        data: data.real_bilance,
        color: '#377eb8',
        negativeColor: '#e41a1c'
 
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Schodky a přebytky českého státního rozpočtu'
            }
          }
        }]
      }
    });
  });


  $(function () {
    Highcharts.chart('graf2', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Schodky a přebytky jako procento z příjmů státního rozpočtu'
      },
      xAxis: {
        categories: data.rok,
        crosshair: true,
        plotBands: [
        {
          from: -0.5,
          to: 4.5,
          color: '#f0f0f0',
          label: {
            text: "Klaus"
          }
        },
        {
          from: 4.5,
          to: 5.5,
          color: '#ffffff',
          label: {
            text: "Tošovský",
            y: -5
          }
        },
        {
          from: 5.5,
          to: 9.5,
          color: '#f0f0f0',
          label: {
            text: "Zeman"
          }
        },
        {
          from: 9.5,
          to: 11.5,
          color: '#ffffff',
          label: {
            text: "Špidla"
          }
        },
        {
          from: 11.5,
          to: 12.5,
          color: '#f0f0f0',
          label: {
            text: "Gross"
          }
        },
        {
          from: 12.5,
          to: 13.5,
          color: '#ffffff',
          label: {
            text: "Paroubek",
            y:-5
          }
        },
        {
          from: 13.5,
          to: 16.5,
          color: '#f0f0f0',
          label: {
            text: "Topolánek"
          }
        },
        ,
        {
          from: 16.5,
          to: 17.5,
          color: '#ffffff',
          label: {
            text: "Fischer",
            y: -5
          }
        },
        {
          from: 17.5,
          to: 20.5,
          color: '#f0f0f0',
          label: {
            text: "Nečas"
          }
        },
        {
          from: 20.5,
          to: 21.5,
          color: '#ffffff',
          label: {
            text: "Rusnok",
            y: -5
          }
        },
        {
          from: 21.5,
          to: 24.5,
          color: '#f0f0f0',
          label: {
            text: "Sobotka"
          }
        }
        ],
      },
      yAxis: {
        title: {
          text: "procent z příjmů rozpočtu"
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      tooltip: {
        shared: true,
        //pointFormat: '<span style="color:{point.color}">\u25CF</span> schodek rozpočtu: <b>{point.y:,.2f}',
        formatter: function() {
          return "Schodek rozpočtu: <b>" + Math.round(this.y*100)/100 + "</b> % příjmů <br>Schodek rozpočtu: <b>" + Math.round((data.real_bilance[data.rok.indexOf(this.x)])*100)/100 + "</b> mld.";
        }
      },      
      plotOptions: {
        column: {
          color: '#377eb8',
          negativeColor: '#e41a1c',
          borderWidth: 0,
        }
      },
      series: [{
        name: 'bilance jako procento z příjmů státního rozpočtu',
        data: data.bilance_pct
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Bilance jako procento z příjmů státního rozpočtu'
            }
          }
        }]
      }
    });
  });


$(function () {
    Highcharts.chart('graf3', {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Srovnání plánované a skutečné bilance státního rozpočtu'
      },
      xAxis: {
        categories: data.rok,
        crosshair: true,
        plotBands: [
        {
          from: -0.5,
          to: 4.5,
          color: '#f0f0f0',
          label: {
            text: "Klaus"
          }
        },
        {
          from: 4.5,
          to: 5.5,
          color: '#ffffff',
          label: {
            text: "Tošovský",
            y: -5
          }
        },
        {
          from: 5.5,
          to: 9.5,
          color: '#f0f0f0',
          label: {
            text: "Zeman"
          }
        },
        {
          from: 9.5,
          to: 11.5,
          color: '#ffffff',
          label: {
            text: "Špidla"
          }
        },
        {
          from: 11.5,
          to: 12.5,
          color: '#f0f0f0',
          label: {
            text: "Gross"
          }
        },
        {
          from: 12.5,
          to: 13.5,
          color: '#ffffff',
          label: {
            text: "Paroubek",
            y:-5
          }
        },
        {
          from: 13.5,
          to: 16.5,
          color: '#f0f0f0',
          label: {
            text: "Topolánek"
          }
        },
        ,
        {
          from: 16.5,
          to: 17.5,
          color: '#ffffff',
          label: {
            text: "Fischer",
            y: -5
          }
        },
        {
          from: 17.5,
          to: 20.5,
          color: '#f0f0f0',
          label: {
            text: "Nečas"
          }
        },
        {
          from: 20.5,
          to: 21.5,
          color: '#ffffff',
          label: {
            text: "Rusnok",
            y: -5
          }
        },
        {
          from: 21.5,
          to: 24.5,
          color: '#f0f0f0',
          label: {
            text: "Sobotka"
          }
        }
        ],
      },
      yAxis: {
        title: {
          text: "miliardy korun"
        }
      },
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      },
      tooltip: {
        shared: true,
        pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:,.2f}</b> mld.<br/>'
      },
      plotOptions: {
        column: {
          borderWidth: 0
        }
      },
      series: [{
        name: 'plánovaná bilance',
        data: data.plan_bilance.slice(0,-2),
        color: '#87B2D4',
        negativeColor: '#F39899'
      },{
        name: 'skutečná bilance',
        data: data.real_bilance.slice(0,-2),
        color: '#377eb8',
        negativeColor: '#e41a1c'
      }],
      responsive: {
        rules: [{
          condition: {
            maxWidth: 600
          },
          chartOptions: {
            xAxis: {
              plotBands: []
            },
            yAxis: {
              title: {
                text: ''
              }
            },
            title: {
              text: 'Schodky a přebytky českého státního rozpočtu'
            }
          }
        }]
      }
    });
  });


$(function () {
Highcharts.chart('graf4', {
chart: {
  type: 'column'
},
title: {
  text: 'Příjmy a výdaje státního rozpočtu'
},
xAxis: {
  categories: data.rok,
  crosshair: true,
  plotBands: [
  {
    from: -0.5,
    to: 4.5,
    color: '#f0f0f0',
    label: {
      text: "Klaus"
    }
  },
  {
    from: 4.5,
    to: 5.5,
    color: '#ffffff',
    label: {
      text: "Tošovský",
      y: -5
    }
  },
  {
    from: 5.5,
    to: 9.5,
    color: '#f0f0f0',
    label: {
      text: "Zeman"
    }
  },
  {
    from: 9.5,
    to: 11.5,
    color: '#ffffff',
    label: {
      text: "Špidla"
    }
  },
  {
    from: 11.5,
    to: 12.5,
    color: '#f0f0f0',
    label: {
      text: "Gross"
    }
  },
  {
    from: 12.5,
    to: 13.5,
    color: '#ffffff',
    label: {
      text: "Paroubek",
      y:-5
    }
  },
  {
    from: 13.5,
    to: 16.5,
    color: '#f0f0f0',
    label: {
      text: "Topolánek"
    }
  },
  ,
  {
    from: 16.5,
    to: 17.5,
    color: '#ffffff',
    label: {
      text: "Fischer",
      y: -5
    }
  },
  {
    from: 17.5,
    to: 20.5,
    color: '#f0f0f0',
    label: {
      text: "Nečas"
    }
  },
  {
    from: 20.5,
    to: 21.5,
    color: '#ffffff',
    label: {
      text: "Rusnok",
      y: -5
    }
  },
  {
    from: 21.5,
    to: 24.5,
    color: '#f0f0f0',
    label: {
      text: "Sobotka"
    }
  }
  ],
},
yAxis: {
  title: {
    text: "miliardy korun"
  }
},
credits: {
  enabled: false
},
legend: {
  enabled: false
},
tooltip: {
  shared: true,
  pointFormat: '<span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y:,.2f}</b> mld.<br/>'
},
plotOptions: {
  column: {
    borderWidth: 0
  }
},
series: [{
  name: 'reálné příjmy',
  data: data.real_prijmy,
  color: '#377eb8'
},{
  name: 'reálné výdaje',
  data: data.real_vydaje,
  color: '#e41a1c'
}],
responsive: {
  rules: [{
    condition: {
      maxWidth: 600
    },
    chartOptions: {
      xAxis: {
        plotBands: []
      },
      yAxis: {
        title: {
          text: ''
        }
      }
    }
  }]
}
});
});